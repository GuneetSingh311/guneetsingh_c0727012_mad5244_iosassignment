//
//  GameScene2.swift
//  CatAcademy
//
//  Created by Guneet on 2019-02-28.
//  Copyright © 2019 Guneet. All rights reserved.
//

import SpriteKit
import GameplayKit
class GameScene2: SKScene {
    var background:SKSpriteNode = SKSpriteNode()
    var monster:SKSpriteNode = SKSpriteNode()
    var monster2:SKSpriteNode = SKSpriteNode()
    var monster3:SKSpriteNode = SKSpriteNode()
    var cat:SKSpriteNode = SKSpriteNode()
    var life:SKSpriteNode = SKSpriteNode()
    var life2:SKSpriteNode = SKSpriteNode()
    var life3:SKSpriteNode = SKSpriteNode()
    var singleleftGesture:SKSpriteNode = SKSpriteNode()
    var doubledownGesture:SKSpriteNode = SKSpriteNode()
    var doubleupGesture:SKSpriteNode = SKSpriteNode()
    var lifeCount = 3
    var tapGesture = UITapGestureRecognizer()
    // MARK: One time swipe gestures variables
    var SwipeOneRightGesture = UISwipeGestureRecognizer()
    var SwipeOneLeftGesture = UISwipeGestureRecognizer()
    var SwipeOneUpGesture = UISwipeGestureRecognizer()
    var SwipeOneDownGesture = UISwipeGestureRecognizer()
    
    // MARK: Two time swipe getures variable
    
    var SwipedTwoRightGesture = UISwipeGestureRecognizer()
    var SwipedTwoLeftGesture = UISwipeGestureRecognizer()
    var SwipedTwoUpGesture = UISwipeGestureRecognizer()
    var SwipedTwoDownGesture = UISwipeGestureRecognizer()
    
    // MARK: Win condition variables to move to next level
    
    var level2gesture1Case = false
    var level2gesture2Case = false
    var level2gesture3case = false
    
    override func didMove(to view: SKView) {
        
        self.monster = self.childNode(withName: "monster") as! SKSpriteNode
        self.monster2 = self.childNode(withName: "monster2") as! SKSpriteNode
        self.monster3 = self.childNode(withName: "monster3") as! SKSpriteNode
        self.cat = self.childNode(withName: "cat") as! SKSpriteNode
        self.background = self.childNode(withName: "background") as! SKSpriteNode
        self.life = self.childNode(withName: "life") as! SKSpriteNode
        self.life2 = self.childNode(withName: "life2") as! SKSpriteNode
        self.life3 = self.childNode(withName: "life3") as! SKSpriteNode
        self.singleleftGesture = self.childNode(withName: "singleleftGesture") as! SKSpriteNode
        self.doubleupGesture = self.childNode(withName: "doubleupGesture") as! SKSpriteNode
        self.doubledownGesture = self.childNode(withName: "doubledownGesture") as! SKSpriteNode
        
        
        let moveMonster = SKAction.moveTo(x: -size.width, duration: 9.0)
        self.monster.run(moveMonster)
        self.monster2.run(moveMonster)
        self.monster3.run(moveMonster)
        self.singleleftGesture.run(moveMonster)
        self.doubleupGesture.run(moveMonster)
        self.doubledownGesture.run(moveMonster)
        
        
        
        
        
        // MARK: TOUCH GESTURES.
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.panGesture(sender:)))
        tapGesture.numberOfTapsRequired = 2
        self.view?.addGestureRecognizer(tapGesture)
        SwipeOneLeftGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeLeft))
        SwipeOneLeftGesture.direction = .left
        SwipeOneLeftGesture.numberOfTouchesRequired = 1
        self.view?.addGestureRecognizer(SwipeOneLeftGesture)
        SwipeOneRightGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRight))
        SwipeOneRightGesture.direction = .right
        SwipeOneRightGesture.numberOfTouchesRequired = 1
        self.view?.addGestureRecognizer(SwipeOneRightGesture)
        SwipeOneUpGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeUp))
        SwipeOneUpGesture.direction = .up
        SwipeOneUpGesture.numberOfTouchesRequired = 1
        self.view?.addGestureRecognizer(SwipeOneUpGesture)
        SwipeOneDownGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeDown))
        SwipeOneDownGesture.direction = .down
        SwipeOneDownGesture.numberOfTouchesRequired = 1
        self.view?.addGestureRecognizer(SwipeOneDownGesture)
        
        
        // TWO SWIPE GESTURES.
        SwipedTwoRightGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeTworight))
        SwipedTwoRightGesture.direction = .right
        SwipedTwoRightGesture.numberOfTouchesRequired = 2
        self.view?.addGestureRecognizer(SwipedTwoRightGesture)
        SwipedTwoLeftGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeTwoleft))
        SwipedTwoLeftGesture.numberOfTouchesRequired = 2
        SwipedTwoLeftGesture.direction = .left
        self.view?.addGestureRecognizer(SwipedTwoLeftGesture)
        SwipedTwoUpGesture = UISwipeGestureRecognizer(target: self, action:#selector(self.swipeTwoup))
        SwipedTwoUpGesture.numberOfTouchesRequired = 2
        SwipedTwoUpGesture.direction = .up
        self.view?.addGestureRecognizer(SwipedTwoUpGesture)
        SwipedTwoDownGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeTwoDown))
        SwipedTwoDownGesture.numberOfTouchesRequired = 2
        SwipedTwoDownGesture.direction = .down
        self.view?.addGestureRecognizer(SwipedTwoDownGesture)
        let audio = SKAction.playSoundFileNamed("arp.mp3", waitForCompletion: true)
        self.cat.run(audio)
    }
    @objc func runCat() {
        self.cat.run(createHitAnimation())
        print("pan")
    }
    
    @objc func createHitAnimation()->SKAction {
        
        //animate the cat going to sleep
        let frame1 = SKTexture.init(imageNamed: "attack1")
        let frame2 = SKTexture.init(imageNamed: "attack2")
        let frame3 = SKTexture.init(imageNamed: "attack3")
        let sleepFrames: [SKTexture] = [frame1, frame2,frame3]
        
        // Change the frame per 0.25 sec
        let animation = SKAction.animate(with: sleepFrames, timePerFrame: 0.1)
        return animation
    }
    
    
    
    
    
    
    // MARK: Gesture Functions.
    @objc func panGesture(sender:UIPanGestureRecognizer)
    {
   
        
    }
    @objc func swipeRight()
    {
        print("SwipeRight")
        
    }
    @objc func swipeLeft()
    {
        print("SwipeLeft")
        self.runCat()
        self.monster.removeFromParent()
        self.singleleftGesture.removeFromParent()
        self.level2gesture1Case = true
        
    }
    @objc func swipeUp()
    {
        print("SwipeUp")
    }
    @objc func swipeDown()
    {
        print("SwipeDown")
    }
    
    @objc  func swipeTworight()
    {
        print("two right")

    }
    
    @objc  func swipeTwoleft()
    {
     
    }
    
    @objc  func swipeTwoup()
    {
        print("two up")
        self.runCat()
        self.monster2.removeFromParent()
        self.doubleupGesture.removeFromParent()
        self.level2gesture2Case = true
    }
    @objc  func swipeTwoDown()
    {
        print("two down")
        self.runCat()
        self.monster3.removeFromParent()
        self.doubledownGesture.removeFromParent()
        self.level2gesture3case = true
    }
    @objc func pinchGesture(sender: UIPinchGestureRecognizer){
        sender.view?.transform = (sender.view?.transform)!.scaledBy(x: sender.scale, y: sender.scale)
        sender.scale = 1
        print("pinch gesture")
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        
        if(self.lifeCount==0)
        {
            Timer.scheduledTimer(withTimeInterval: 4.0, repeats: false) { (Timer) in
                let scene = GameScene(fileNamed:"GameOver.sks")
                scene!.scaleMode = self.scaleMode
                self.view?.presentScene(scene)
                
            }
        }
        if(self.cat.intersects(self.monster))
        {
            self.life.removeFromParent()
            self.lifeCount = self.lifeCount - 1
        }
        if(self.cat.intersects(self.monster2))
        {
            self.life2.removeFromParent()
            self.lifeCount = self.lifeCount - 1
        }
        if(self.cat.intersects(self.monster3))
        {
            self.life3.removeFromParent()
            self.lifeCount = self.lifeCount - 1
        }
        if(self.level2gesture2Case==true&&self.level2gesture1Case == true && self.level2gesture3case==true)
        {     
            Timer.scheduledTimer(withTimeInterval: 4.0, repeats: false) { (Timer) in
                let scene = GameScene(fileNamed:"GameScene\(3).sks")
                scene!.scaleMode = self.scaleMode
                self.view?.presentScene(scene)
                
            }
            print("win")
        }
        
        
        
    }
    
    
    
    
    
    
    
    
    
}
