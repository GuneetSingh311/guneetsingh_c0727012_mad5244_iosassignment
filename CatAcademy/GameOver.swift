//
//  GameScene2.swift
//  CatAcademy
//
//  Created by Guneet on 2019-02-28.
//  Copyright © 2019 Guneet. All rights reserved.
//

import SpriteKit
import GameplayKit
class GameOver: SKScene {
    var background:SKSpriteNode = SKSpriteNode()
    var label:SKLabelNode = SKLabelNode()
    
    
    override func didMove(to view: SKView) {
    
        self.background = self.childNode(withName: "background") as! SKSpriteNode
        self.label = self.childNode(withName: "playAgain") as! SKLabelNode
    }
  
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        let location = touch?.location(in: self)
       let nodeTouched = self.atPoint(location!)
        
        if(nodeTouched.name=="playAgain")
        {
            let scene = GameScene(fileNamed:"GameScene1.sks")
            scene!.scaleMode = self.scaleMode
            self.view?.presentScene(scene)
        }
    }
    
    
    
    
    
    
}
