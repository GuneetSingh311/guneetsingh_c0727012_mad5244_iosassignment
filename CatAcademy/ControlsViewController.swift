//
//  ControlsViewController.swift
//  CatAcademy
//
//  Created by Guneet on 2019-03-02.
//  Copyright © 2019 Guneet. All rights reserved.
//

import UIKit

class ControlsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
   
    var controlsImage:[String] = ["upSingle.png","downSingle.png","rightSingle.png","leftSingle.png",
                                   "upDouble.png","downDouble.png","rightDouble.png","leftDouble.png"]
    var controlsDescription:[String] = ["Swipe up with one finger","Swipe down with one finger","Swipe right with one finger",
                                        "Swipe left with one finger","Swipe up with two fingers","Swipe down with two fingers",
                                        "Swipe right with two fingers","Swipe left with two fingers"]

    @IBOutlet weak var controlsTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        controlsTable.delegate=self
        controlsTable.dataSource=self
        controlsTable.reloadData()
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return controlsImage.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)as! customTableViewCell
        cell.controlImage.image = UIImage(named: controlsImage[indexPath.row])
        cell.controlLabel.text = controlsDescription[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
