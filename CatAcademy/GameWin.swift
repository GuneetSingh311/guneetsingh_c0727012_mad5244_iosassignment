//
//  GameScene2.swift
//  CatAcademy
//
//  Created by Guneet on 2019-02-28.
//  Copyright © 2019 Guneet. All rights reserved.
//

import SpriteKit
import GameplayKit
class GameWin: SKScene {
    var background:SKSpriteNode = SKSpriteNode()
    var cat:SKSpriteNode = SKSpriteNode()
    var label:SKLabelNode = SKLabelNode()
    
    
    override func didMove(to view: SKView) {
        
        self.background = self.childNode(withName: "background") as! SKSpriteNode
        self.label = self.childNode(withName: "playAgain") as! SKLabelNode
        self.cat = self.childNode(withName: "cat") as! SKSpriteNode
        let action1 = SKAction.moveTo(x: 200, duration: 9.0)
        let action2 = self.createHitAnimation()
        self.cat.run(SKAction.sequence([action1,action2]))
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        let location = touch?.location(in: self)
        let nodeTouched = self.atPoint(location!)
        
        if(nodeTouched.name=="playAgain")
        {
            let scene = GameScene(fileNamed:"GameScene1.sks")
            scene!.scaleMode = self.scaleMode
            self.view?.presentScene(scene)
        }
    }
    
    @objc func createHitAnimation()->SKAction {
        
        //animate the cat going to sleep
        let frame1 = SKTexture.init(imageNamed: "attack1")
        let frame2 = SKTexture.init(imageNamed: "attack2")
        let frame3 = SKTexture.init(imageNamed: "attack3")
        let sleepFrames: [SKTexture] = [frame1, frame2,frame3]
        
        // Change the frame per 0.25 sec
        let animation = SKAction.animate(with: sleepFrames, timePerFrame: 0.1)
        return animation
    }
    
    
    
    
}

